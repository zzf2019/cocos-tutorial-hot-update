var fs = require('fs');
var path = require('path');
var crypto = require('crypto');

var manifest = {
    packageUrl: 'http://localhost/tutorial-hot-update/remote-assets/',
    remoteManifestUrl: 'http://localhost/tutorial-hot-update/remote-assets/project.manifest',
    remoteVersionUrl: 'http://localhost/tutorial-hot-update/remote-assets/version.manifest',
    version: '1.0.0',
    assets: {},
    searchPaths: []
};

var dest = './remote-assets/';
var src = './jsb/';

// Parse arguments
var i = 2;
while (i < process.argv.length) {
    var arg = process.argv[i];

    switch (arg) {
        case '--url':
        case '-u':
            var url = process.argv[i + 1];
            manifest.packageUrl = url;
            manifest.remoteManifestUrl = url + 'project.manifest';
            manifest.remoteVersionUrl = url + 'version.manifest';
            i += 2;
            break;
        case '--version':
        case '-v':
            manifest.version = process.argv[i + 1];
            i += 2;
            break;
        case '--src':
        case '-s':
            src = process.argv[i + 1];
            i += 2;
            break;
        case '--dest':
        case '-d':
            dest = process.argv[i + 1];
            i += 2;
            break;
        default:
            i++;
            break;
    }
}


function readDirOrFile(dirOrFilePath, obj) {
    try {
        var stat = fs.statSync(dirOrFilePath);
        var size, md5, compressed, relative;
        var fileDataMake = function (filePath) {
            // Size in Bytes
            size = stat['size'];
            md5 = crypto.createHash('md5').update(fs.readFileSync(filePath)).digest('hex');
            compressed = path.extname(filePath).toLowerCase() === '.zip';

            relative = path.relative(src, filePath);
            relative = relative.replace(/\\/g, '/');
            relative = encodeURI(relative);
            obj[relative] = {
                'size': size,
                'md5': md5
            };
            if (compressed) {
                obj[relative].compressed = true;
            }
        }
        if (stat.isDirectory()) {
            var subpaths = fs.readdirSync(dirOrFilePath), subpath;
            for (var i = 0; i < subpaths.length; ++i) {
                if (subpaths[i][0] === '.') {
                    continue;
                }
                subpath = path.join(dirOrFilePath, subpaths[i]);
                var sub = fs.statSync(subpath);
                if (sub.isDirectory()) {
                    readDirOrFile(subpath, obj);
                }
                else if (sub.isFile()) {
                    fileDataMake(subpath);
                }
            }
        } else if (stat.isFile()){
            fileDataMake(dirOrFilePath);
        }
    } catch (err) {
        console.error(err)
    }
}

var mkdirSync = function (path) {
    try {
        fs.mkdirSync(path);
    } catch (e) {
        if (e.code != 'EEXIST') throw e;
    }
}

// Iterate src、assets、jsb-adapter folder and main.js file
readDirOrFile(path.join(src, 'src'), manifest.assets);
readDirOrFile(path.join(src, 'assets'), manifest.assets);
readDirOrFile(path.join(src, 'main.js'), manifest.assets);
readDirOrFile(path.join(src, 'jsb-adapter'), manifest.assets);

var destManifest = path.join(dest, 'project.manifest');
var destVersion = path.join(dest, 'version.manifest');

mkdirSync(dest);

fs.writeFile(destManifest, JSON.stringify(manifest), (err) => {
    if (err) throw err;
    console.log('Manifest successfully generated');
});

delete manifest.assets;
delete manifest.searchPaths;
fs.writeFile(destVersion, JSON.stringify(manifest), (err) => {
    if (err) throw err;
    console.log('Version successfully generated');
});
